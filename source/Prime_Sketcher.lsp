; Prime Sketcher
; Rappresentazione numeri primi
;
; Autore    : Roberto Rossi
; Web       : http://www.redchar.net
; Versione  : 1.0
;
; Algoritmo iniziale utilizzato :       http://www.235711.org/grafico-distribuzione-il-sentiero-dei-numeri-primi/
; Elenco numeri primo utilizzabili :    http://primes.utm.edu/lists/small/millions/
;                                       http://www.primos.mat.br/indexen.html
;
; The MIT License (MIT)
;
; Copyright (c) 2015 Roberto Rossi
;
; Permission is hereby granted, free of charge, to any person obtaining a copy
; of this software and associated documentation files (the "Software"), to deal
; in the Software without restriction, including without limitation the rights
; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
; copies of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included in
; all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
; THE SOFTWARE.


;divide una linea letta dal file di dati in X parti, una parte per ogni numero
;   usa le tabulazioni e gli spazi come separatori e riconosce solamente i numeri
;   se la linea contiene anche solo un carattere diverso sa un numero, uno spazio
;   p una tabulazione, viene ritornato nil
(defun decodeLinePF ( line / result lenField prev_ch ch num result i otherChars)
    (setq otherChars nil)
    (setq i 1)
    (while (/= (setq ch (substr line i 1)) "")
        (if (and (/= ch " ") (/= ch "\t"))
            (progn
                (if (or (< (ascii ch) 48) (> (ascii ch) 57))
                    (setq otherChars t) ;c'� un carattere che non � un numero
                )
                
                (setq num (strcat num ch))
            )
            (progn
                (if (and num (/= num ""))
                    (setq result (cons (atof num) result))
                )
                (setq num "")
            )
        )
        (setq prev_ch ch)
        (setq i (1+ i))
    )
    
    (if (and num (/= num ""))
        (setq result (cons (atof num) result))
    )
    
    (if otherChars
        (setq result nil)
    )

    (if result
        (reverse result)
        result
    )
)

;Legge il file di dati e lo disegna
(defun readPF (filename viewStep nPrimes / line idf listN n count continue)
    (setq idf (open filename "r"))
    (setq count 0)
    (setq continue t)
    (if idf
        (progn
            (princ (strcat "\nRappresentazione numeri primi" 
                           "\n        Visualizzazione dati ogni " (itoa viewStep) " numeri"
                           "\n        Numeri da rappresentare " (itoa nPrimes)
                    )
            )
            (while (and continue (setq line (read-line idf)))
                (if (/= line "")
                    (progn
                        (setq listN (decodeLinePF line))
                        (foreach n listN
                            (if (> n 0)
                                (progn                                    
                                    (draw_PN n)
                                    (setq count (1+ count))
                                    
                                    (if (= (rem count viewStep) 0)
                                        (princ (strcat "\nNumeri primi disegnati: " (itoa count) ". Ultimo numero primo :" (if public_last_n (rtos public_last_n) "0")))
                                    )
                                    
                                );endp
                            )
                            ;(draw_PN n)
                        );endfor
                        
                        
                        (if (> count nPrimes);limite
                            (progn
                                (setq continue nil)
                            );endp    
                        );endif
                    );Endp
                );endif
            );endw
            (princ (strcat "\nSono stati disegnati " (itoa count) " numeri primi"))            
            (close idf)
        )
    )
(prin1)
)

;impostazioni variabili globali
(defun sketcherDataReset ( / )
    (setq public_last_n nil)
    (setq public_move_x t) ;direzione iniziale
    (setq public_x_position 0.0)
    (setq public_y_position 0.0)
    (setq public_multiplier 1)
)

;disegna le necessarie entit� per il numero primo specificato
(defun draw_PN (n / x y pt1 pt2 pt1a pt2a)
    (if (not public_last_n)
        (setq public_last_n n)
        (progn
            (setq x public_x_position)
            (setq y public_y_position)
            (if public_move_x
                (progn
                    (setq x (+ x (* (- n public_last_n) public_multiplier)))
                )
                (progn
                    (setq y (+ y (* (- n public_last_n) public_multiplier)))
                    (setq public_multiplier (* public_multiplier -1))
                )
            )
            (setq public_move_x (not public_move_x))
            
            (drawline x y public_x_position public_y_position)
            (setq public_last_n n)
            (setq public_x_position x)
            (setq public_y_position y)
        )
    );endif   
(prin1)
)

;disegna un punto
(defun drawpoint ( x y / )
    (entmake (list (cons 0 "POINT") (list 10 x y 0.0)))
    (prin1)
)

;disegna una linea tra due punti
(defun drawline ( x y x1 y1 / )
    (entmake (list (cons 0 "LINE") (list 10 x y 0.0) (list 11 x1 y1 0.0)))
    (prin1)
)

;richiede il file .txt dei dati all'utente
(defun sketcherDialogSelFile ( input / result)
    (setq result (getfiled "Selezionare file dati" input "txt" 0))
    (if (not result)
        (setq result input)
    )
result
)

;Visualizza e gestisce la maschera delle opzioni.
(defun sketcherDialog (/ dcl_id result filebase datafile
                         nprimes steps)
    (setq datafile "primes1.txt")
    (setq filebase "primeSketcher.dcl")
    (setq dcl_id (load_dialog filebase)) 
    (if (not (new_dialog "primeSk" dcl_id)) 
        (progn
            (alert (strcat "Non � stato trovato il file '" filebase "' nei percorsi di ricerca!"
                            "\nAggiungere il percorso dei file usando la maschera delle Opzioni."))
            (exit) 
        )
    )
    
    (set_tile "fileTxt" datafile)
    (set_tile "nPrimes" "2000")
    (set_tile "steps" "500")
    (action_tile "browser" "(set_tile \"fileTxt\" (sketcherDialogSelFile (get_tile \"fileTxt\")))") 
    (action_tile "cancel" "(done_dialog 0)") 
    (action_tile "accept" "(setq nprimes (get_tile \"nPrimes\"))(setq steps (get_tile \"steps\"))(setq datafile (get_tile \"fileTxt\")) (done_dialog 1)") 
    (setq result (start_dialog))
    (if (= result 1) 
        (progn
            (setq result (list (cons "nprimes" (atoi nprimes))(cons "steps" (atoi steps))(cons "file" datafile)))
        ) 
        (setq result nil)
    ) 
    (unload_dialog dcl_id)     

result
)

;comando principale
(defun c:PrimeSketcher (/ filename filebase data steps nprimes)
    (sketcherDataReset)
    (setq data (sketcherDialog))
    (if data
        (progn
            (setq filebase (cdr (assoc "file" data)))
            (setq steps (cdr (assoc "steps" data)))
            (setq nprimes (cdr (assoc "nprimes" data)))

            (setq filename (findfile filebase))
            (if filename
                (progn
                    ;Uso
                    ; (readPF [nome file] [dopo n numeri mostra dati] [numeri da disegnare])
                    (readPF     filename
                                steps
                                nprimes
                                )
                    (princ "\n\nProcedura conclusa con successo.")
                );endp    
                (progn
                    (alert (strcat "Non � stato trovato il file '" filebase "'!"
                                    "\nSe si specifica il nome file senza percorso, � necessario aggiungere il la posizione del file usando la maschera delle Opzioni, nei 'percorsi di ricerca'."))
                )
            );endif
        );endp
    );endif

(prin1)
);enddef

(prin1)