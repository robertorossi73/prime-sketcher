﻿# Prime Sketcher

![Prime Sketcher](http://www.redchar.net/pages/user/pages/2018/prime-sketcher/title.jpg)

Tutto parte per gioco, dai **numeri primi**.

Questo progetto potrebbe essere inutile, oppure potrebbe essere un sistema per
esplorare un aspetto affascinante del mondo dei numeri. Ma potrebbe anche
essere solamente un sistema per creare immagini simpatiche. Per me è un
**progetto curioso e rilassante**.

Tutto nasce dopo la lettura di un articolo che spiega un semplice algoritmo di
disegno basato sui numeri primi.

Da tale articolo è nato Prime Sketcher, un software che ha lo scopo di
rappresentare graficamente, in varie forme, i numeri primi.

Con semplici numeri, utilizzando un comune software CAD unito a Prime
Sketcher, sarà possibile disegnare simpatiche e curiose forme geometriche.

Per approfondimenti:

<http://www.redchar.net/?x=entry:entry151012-201312>
